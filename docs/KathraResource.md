
# KathraResource

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** | id |  [optional]
**name** | **String** | name |  [optional]
**metadata** | **Map&lt;String, Object&gt;** | metadata |  [optional]



