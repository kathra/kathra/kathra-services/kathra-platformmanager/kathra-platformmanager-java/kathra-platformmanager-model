
# KathraEnvironment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**resources** | [**List&lt;KathraEnvironmentResource&gt;**](KathraEnvironmentResource.md) | resources |  [optional]
**status** | [**StatusEnum**](#StatusEnum) | status |  [optional]


<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
DEPLOYING | &quot;deploying&quot;
AVAILABLE | &quot;available&quot;
DELETING | &quot;deleting&quot;
FAILSAFE | &quot;failsafe&quot;
ERROR | &quot;error&quot;



