
# KathraCatalogResourceParameterDependency

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | name |  [optional]
**condition** | [**ConditionEnum**](#ConditionEnum) | condition |  [optional]
**value** | **String** | value |  [optional]


<a name="ConditionEnum"></a>
## Enum: ConditionEnum
Name | Value
---- | -----
IS | &quot;is&quot;
ISNOT | &quot;isNot&quot;



