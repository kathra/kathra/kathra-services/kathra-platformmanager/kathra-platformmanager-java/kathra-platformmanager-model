
# KathraCatalogResourceParameter

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | name |  [optional]
**optional** | **Boolean** | optional |  [optional]
**type** | [**TypeEnum**](#TypeEnum) | type |  [optional]
**defaultValue** | **String** | defaultValue |  [optional]
**description** | **String** | description |  [optional]
**label** | **String** | label |  [optional]
**rules** | [**List&lt;KathraCatalogResourceParameterValidationRule&gt;**](KathraCatalogResourceParameterValidationRule.md) | rules |  [optional]
**depends** | [**List&lt;KathraCatalogResourceParameterDependency&gt;**](KathraCatalogResourceParameterDependency.md) | depends |  [optional]


<a name="TypeEnum"></a>
## Enum: TypeEnum
Name | Value
---- | -----
TEXT | &quot;text&quot;
FLOAT | &quot;float&quot;
INTEGER | &quot;integer&quot;
SECRET | &quot;secret&quot;
BOOLEAN | &quot;boolean&quot;
SELECT | &quot;select&quot;



