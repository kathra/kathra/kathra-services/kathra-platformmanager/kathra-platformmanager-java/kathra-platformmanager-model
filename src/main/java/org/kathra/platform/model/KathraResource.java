/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.platform.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
* KathraResource
*/
@ApiModel(description = "KathraResource")
public class KathraResource {

    protected String id = null;
    protected String name = null;
    protected Map<String, Object> metadata = null;

    /**
    * Quick Set id
    * Useful setter to use in builder style (eg. myKathraResource.id(Long).anotherQuickSetter(..))
    * @param String id
    * @return KathraResource The modified KathraResource object
    **/
    public KathraResource id(String id) {
        this.id = id;
        return this;
    }

    /**
    * id
    * @return Long id
    **/
    @ApiModelProperty(value = "id")
    public String getId() {
        return id;
    }

    /**
    * id
    **/
    public void setId(String id) {
        this.id = id;
    }

    /**
    * Quick Set name
    * Useful setter to use in builder style (eg. myKathraResource.name(String).anotherQuickSetter(..))
    * @param String name
    * @return KathraResource The modified KathraResource object
    **/
    public KathraResource name(String name) {
        this.name = name;
        return this;
    }

    /**
    * name
    * @return String name
    **/
    @ApiModelProperty(value = "name")
    public String getName() {
        return name;
    }

    /**
    * name
    **/
    public void setName(String name) {
        this.name = name;
    }

    /**
    * Quick Set metadata
    * Useful setter to use in builder style (eg. myKathraResource.metadata(Map<String, Object>).anotherQuickSetter(..))
    * @param Map<String, Object> metadata
    * @return KathraResource The modified KathraResource object
    **/
    public KathraResource metadata(Map<String, Object> metadata) {
        this.metadata = metadata;
        return this;
    }

    public KathraResource putMetadataItem(String key, Object metadataItem) {
        if (this.metadata == null) {
            this.metadata = new HashMap<String, Object>();
        }
        this.metadata.put(key, metadataItem);
        return this;
    }

    /**
    * metadata
    * @return Map<String, Object> metadata
    **/
    @ApiModelProperty(value = "metadata")
    public Map<String, Object> getMetadata() {
        return metadata;
    }

    /**
    * metadata
    **/
    public void setMetadata(Map<String, Object> metadata) {
        this.metadata = metadata;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        KathraResource kathraResource = (KathraResource) o;
        return Objects.equals(this.id, kathraResource.id) &&
        Objects.equals(this.name, kathraResource.name) &&
        Objects.equals(this.metadata, kathraResource.metadata);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, metadata);
    }
}

