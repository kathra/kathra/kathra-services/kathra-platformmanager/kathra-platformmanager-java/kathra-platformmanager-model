/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.platform.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
* KathraCatalogResourceParameterDependency
*/
@ApiModel(description = "KathraCatalogResourceParameterDependency")
public class KathraCatalogResourceParameterDependency {

    protected String name = null;
    /**
    * condition
    */
    public enum ConditionEnum {
        @SerializedName("is")
        IS("is"),
        @SerializedName("isNot")
        ISNOT("isNot");

        private String value;

        ConditionEnum(String value) {
          this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
        public static ConditionEnum fromValue(String text) {
            for (ConditionEnum b : ConditionEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            return null;
        }
    }

    protected ConditionEnum condition = null;
    protected String value = null;

    /**
    * Quick Set name
    * Useful setter to use in builder style (eg. myKathraCatalogResourceParameterDependency.name(String).anotherQuickSetter(..))
    * @param String name
    * @return KathraCatalogResourceParameterDependency The modified KathraCatalogResourceParameterDependency object
    **/
    public KathraCatalogResourceParameterDependency name(String name) {
        this.name = name;
        return this;
    }

    /**
    * name
    * @return String name
    **/
    @ApiModelProperty(value = "name")
    public String getName() {
        return name;
    }

    /**
    * name
    **/
    public void setName(String name) {
        this.name = name;
    }

    /**
    * Quick Set condition
    * Useful setter to use in builder style (eg. myKathraCatalogResourceParameterDependency.condition(ConditionEnum).anotherQuickSetter(..))
    * @param ConditionEnum condition
    * @return KathraCatalogResourceParameterDependency The modified KathraCatalogResourceParameterDependency object
    **/
    public KathraCatalogResourceParameterDependency condition(ConditionEnum condition) {
        this.condition = condition;
        return this;
    }

    /**
    * condition
    * @return ConditionEnum condition
    **/
    @ApiModelProperty(value = "condition")
    public ConditionEnum getCondition() {
        return condition;
    }

    /**
    * condition
    **/
    public void setCondition(ConditionEnum condition) {
        this.condition = condition;
    }

    /**
    * Quick Set value
    * Useful setter to use in builder style (eg. myKathraCatalogResourceParameterDependency.value(String).anotherQuickSetter(..))
    * @param String value
    * @return KathraCatalogResourceParameterDependency The modified KathraCatalogResourceParameterDependency object
    **/
    public KathraCatalogResourceParameterDependency value(String value) {
        this.value = value;
        return this;
    }

    /**
    * value
    * @return String value
    **/
    @ApiModelProperty(value = "value")
    public String getValue() {
        return value;
    }

    /**
    * value
    **/
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        KathraCatalogResourceParameterDependency kathraCatalogResourceParameterDependency = (KathraCatalogResourceParameterDependency) o;
        return Objects.equals(this.name, kathraCatalogResourceParameterDependency.name) &&
        Objects.equals(this.condition, kathraCatalogResourceParameterDependency.condition) &&
        Objects.equals(this.value, kathraCatalogResourceParameterDependency.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, condition, value);
    }
}

