/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.platform.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
* KathraCatalogResourceParameter
*/
@ApiModel(description = "KathraCatalogResourceParameter")
public class KathraCatalogResourceParameter {

    protected String name = null;
    protected Boolean optional = null;
    /**
    * type
    */
    public enum TypeEnum {
        @SerializedName("text")
        TEXT("text"),
        @SerializedName("float")
        FLOAT("float"),
        @SerializedName("integer")
        INTEGER("integer"),
        @SerializedName("secret")
        SECRET("secret"),
        @SerializedName("boolean")
        BOOLEAN("boolean"),
        @SerializedName("select")
        SELECT("select");

        private String value;

        TypeEnum(String value) {
          this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
        public static TypeEnum fromValue(String text) {
            for (TypeEnum b : TypeEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            return null;
        }
    }

    protected TypeEnum type = null;
    protected String defaultValue = null;
    protected String description = null;
    protected String label = null;
    protected List<KathraCatalogResourceParameterValidationRule> rules = null;
    protected List<KathraCatalogResourceParameterDependency> depends = null;

    /**
    * Quick Set name
    * Useful setter to use in builder style (eg. myKathraCatalogResourceParameter.name(String).anotherQuickSetter(..))
    * @param String name
    * @return KathraCatalogResourceParameter The modified KathraCatalogResourceParameter object
    **/
    public KathraCatalogResourceParameter name(String name) {
        this.name = name;
        return this;
    }

    /**
    * name
    * @return String name
    **/
    @ApiModelProperty(value = "name")
    public String getName() {
        return name;
    }

    /**
    * name
    **/
    public void setName(String name) {
        this.name = name;
    }

    /**
    * Quick Set optional
    * Useful setter to use in builder style (eg. myKathraCatalogResourceParameter.optional(Boolean).anotherQuickSetter(..))
    * @param Boolean optional
    * @return KathraCatalogResourceParameter The modified KathraCatalogResourceParameter object
    **/
    public KathraCatalogResourceParameter optional(Boolean optional) {
        this.optional = optional;
        return this;
    }

    /**
    * optional
    * @return Boolean optional
    **/
    @ApiModelProperty(value = "optional")
    public Boolean isOptional() {
        return optional;
    }

    /**
    * optional
    **/
    public void setOptional(Boolean optional) {
        this.optional = optional;
    }

    /**
    * Quick Set type
    * Useful setter to use in builder style (eg. myKathraCatalogResourceParameter.type(TypeEnum).anotherQuickSetter(..))
    * @param TypeEnum type
    * @return KathraCatalogResourceParameter The modified KathraCatalogResourceParameter object
    **/
    public KathraCatalogResourceParameter type(TypeEnum type) {
        this.type = type;
        return this;
    }

    /**
    * type
    * @return TypeEnum type
    **/
    @ApiModelProperty(value = "type")
    public TypeEnum getType() {
        return type;
    }

    /**
    * type
    **/
    public void setType(TypeEnum type) {
        this.type = type;
    }

    /**
    * Quick Set defaultValue
    * Useful setter to use in builder style (eg. myKathraCatalogResourceParameter.defaultValue(String).anotherQuickSetter(..))
    * @param String defaultValue
    * @return KathraCatalogResourceParameter The modified KathraCatalogResourceParameter object
    **/
    public KathraCatalogResourceParameter defaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
        return this;
    }

    /**
    * defaultValue
    * @return String defaultValue
    **/
    @ApiModelProperty(value = "defaultValue")
    public String getDefaultValue() {
        return defaultValue;
    }

    /**
    * defaultValue
    **/
    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    /**
    * Quick Set description
    * Useful setter to use in builder style (eg. myKathraCatalogResourceParameter.description(String).anotherQuickSetter(..))
    * @param String description
    * @return KathraCatalogResourceParameter The modified KathraCatalogResourceParameter object
    **/
    public KathraCatalogResourceParameter description(String description) {
        this.description = description;
        return this;
    }

    /**
    * description
    * @return String description
    **/
    @ApiModelProperty(value = "description")
    public String getDescription() {
        return description;
    }

    /**
    * description
    **/
    public void setDescription(String description) {
        this.description = description;
    }

    /**
    * Quick Set label
    * Useful setter to use in builder style (eg. myKathraCatalogResourceParameter.label(String).anotherQuickSetter(..))
    * @param String label
    * @return KathraCatalogResourceParameter The modified KathraCatalogResourceParameter object
    **/
    public KathraCatalogResourceParameter label(String label) {
        this.label = label;
        return this;
    }

    /**
    * label
    * @return String label
    **/
    @ApiModelProperty(value = "label")
    public String getLabel() {
        return label;
    }

    /**
    * label
    **/
    public void setLabel(String label) {
        this.label = label;
    }

    /**
    * Quick Set rules
    * Useful setter to use in builder style (eg. myKathraCatalogResourceParameter.rules(List<KathraCatalogResourceParameterValidationRule>).anotherQuickSetter(..))
    * @param List<KathraCatalogResourceParameterValidationRule> rules
    * @return KathraCatalogResourceParameter The modified KathraCatalogResourceParameter object
    **/
    public KathraCatalogResourceParameter rules(List<KathraCatalogResourceParameterValidationRule> rules) {
        this.rules = rules;
        return this;
    }

    public KathraCatalogResourceParameter addRulesItem(KathraCatalogResourceParameterValidationRule rulesItem) {
        if (this.rules == null) {
            this.rules = new ArrayList<KathraCatalogResourceParameterValidationRule>();
        }
        this.rules.add(rulesItem);
        return this;
    }

    /**
    * rules
    * @return List<KathraCatalogResourceParameterValidationRule> rules
    **/
    @ApiModelProperty(value = "rules")
    public List<KathraCatalogResourceParameterValidationRule> getRules() {
        return rules;
    }

    /**
    * rules
    **/
    public void setRules(List<KathraCatalogResourceParameterValidationRule> rules) {
        this.rules = rules;
    }

    /**
    * Quick Set depends
    * Useful setter to use in builder style (eg. myKathraCatalogResourceParameter.depends(List<KathraCatalogResourceParameterDependency>).anotherQuickSetter(..))
    * @param List<KathraCatalogResourceParameterDependency> depends
    * @return KathraCatalogResourceParameter The modified KathraCatalogResourceParameter object
    **/
    public KathraCatalogResourceParameter depends(List<KathraCatalogResourceParameterDependency> depends) {
        this.depends = depends;
        return this;
    }

    public KathraCatalogResourceParameter addDependsItem(KathraCatalogResourceParameterDependency dependsItem) {
        if (this.depends == null) {
            this.depends = new ArrayList<KathraCatalogResourceParameterDependency>();
        }
        this.depends.add(dependsItem);
        return this;
    }

    /**
    * depends
    * @return List<KathraCatalogResourceParameterDependency> depends
    **/
    @ApiModelProperty(value = "depends")
    public List<KathraCatalogResourceParameterDependency> getDepends() {
        return depends;
    }

    /**
    * depends
    **/
    public void setDepends(List<KathraCatalogResourceParameterDependency> depends) {
        this.depends = depends;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        KathraCatalogResourceParameter kathraCatalogResourceParameter = (KathraCatalogResourceParameter) o;
        return Objects.equals(this.name, kathraCatalogResourceParameter.name) &&
        Objects.equals(this.optional, kathraCatalogResourceParameter.optional) &&
        Objects.equals(this.type, kathraCatalogResourceParameter.type) &&
        Objects.equals(this.defaultValue, kathraCatalogResourceParameter.defaultValue) &&
        Objects.equals(this.description, kathraCatalogResourceParameter.description) &&
        Objects.equals(this.label, kathraCatalogResourceParameter.label) &&
        Objects.equals(this.rules, kathraCatalogResourceParameter.rules) &&
        Objects.equals(this.depends, kathraCatalogResourceParameter.depends);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, optional, type, defaultValue, description, label, rules, depends);
    }
}

