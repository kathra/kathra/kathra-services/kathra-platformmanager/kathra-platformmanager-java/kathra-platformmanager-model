/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.platform.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
* KathraEnvironmentResourceParameter
*/
@ApiModel(description = "KathraEnvironmentResourceParameter")
public class KathraEnvironmentResourceParameter {

    protected String name = null;
    protected String value = null;
    /**
    * type
    */
    public enum TypeEnum {
        @SerializedName("integer")
        INTEGER("integer"),

        @SerializedName("string")
        STRING("string"),

        @SerializedName("float")
        FLOAT("float"),

        @SerializedName("boolean")
        BOOLEAN("boolean");

        private String value;

        TypeEnum(String value) {
          this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }
        public static TypeEnum fromValue(String text) {
            for (TypeEnum b : TypeEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            return null;
        }
    }

    protected TypeEnum type = null;

    /**
    * Quick Set name
    * Useful setter to use in builder style (eg. myKathraEnvironmentResourceParameter.name(String).anotherQuickSetter(..))
    * @param String name
    * @return KathraEnvironmentResourceParameter The modified KathraEnvironmentResourceParameter object
    **/
    public KathraEnvironmentResourceParameter name(String name) {
        this.name = name;
        return this;
    }

    /**
    * name
    * @return String name
    **/
    @ApiModelProperty(value = "name")
    public String getName() {
        return name;
    }

    /**
    * name
    **/
    public void setName(String name) {
        this.name = name;
    }

    /**
    * Quick Set value
    * Useful setter to use in builder style (eg. myKathraEnvironmentResourceParameter.value(String).anotherQuickSetter(..))
    * @param String value
    * @return KathraEnvironmentResourceParameter The modified KathraEnvironmentResourceParameter object
    **/
    public KathraEnvironmentResourceParameter value(String value) {
        this.value = value;
        return this;
    }

    /**
    * value
    * @return String value
    **/
    @ApiModelProperty(value = "value")
    public String getValue() {
        return value;
    }

    /**
    * value
    **/
    public void setValue(String value) {
        this.value = value;
    }

    /**
    * Quick Set type
    * Useful setter to use in builder style (eg. myKathraEnvironmentResourceParameter.type(TypeEnum).anotherQuickSetter(..))
    * @param TypeEnum type
    * @return KathraEnvironmentResourceParameter The modified KathraEnvironmentResourceParameter object
    **/
    public KathraEnvironmentResourceParameter type(TypeEnum type) {
        this.type = type;
        return this;
    }

    /**
    * type
    * @return TypeEnum type
    **/
    @ApiModelProperty(value = "type")
    public TypeEnum getType() {
        return type;
    }

    /**
    * type
    **/
    public void setType(TypeEnum type) {
        this.type = type;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        KathraEnvironmentResourceParameter kathraEnvironmentResourceParameter = (KathraEnvironmentResourceParameter) o;
        return Objects.equals(this.name, kathraEnvironmentResourceParameter.name) &&
        Objects.equals(this.value, kathraEnvironmentResourceParameter.value) &&
        Objects.equals(this.type, kathraEnvironmentResourceParameter.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, value, type);
    }
}

